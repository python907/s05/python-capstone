from abc import ABC, abstractclassmethod

class Person(ABC):
	@abstractclassmethod
	def getFullName(self, firstName, lastName):
		pass

	def addRequest(self):
		pass

	def checkRequest(self):
		pass

	def addUser(self, firstName, lastName, email, department):
		pass

class Employee(Person):
	def __init__(self, firstName, lastName, email, department):
		super().__init__()
		self._firstName = firstName
		self._lastName = lastName
		self._email = email
		self._department = department

	# getter of Employee class
	def get_firstName(self):
		return self._firstName

	def get_lastName(self):
		return self._lastName

	def get_email(self):
		return self._email

	def get_department(self):
		return self._department


	#setters of Employee class
	def set_firstName(self):
		self._firstName = firstName

	def set_lastName(self):
		self._lastName = lastName

	def set_email(self):
		self._email = email

	def set_department(self):
		self._department = department

# details method
	def get_details(self):
		print(f"{self._email} has logged in")

employee1 = Employee("John", "Doe", "djohn@mail.com", "Marketing")
employee1.get_details()



# class TeamLead():
# 	def occupation(self):
# 		print("Team Lead")

# 	def hasAuth(self):
# 		print(True)

# class TeamMember():
# 	def occupation(self):
# 		print("Team Member")

# 	def hasAuth(self):
# 		print(False)

# team_lead = TeamLead()
# team_member = TeamMember()

# for person in (team_lead, team_member):
# 	person.occupation()
# 	person.hasAuth()




# Test Case:

# employee1 = Employee("John", "Doe", "djohn@mail.com", "Marketing")
# employee2 = Employee("Jane", "Smith", "sjane@mail.com", "Marketing")
# employee3 = Employee("Robert", "Patterson", "probert@mail.com", "Sales")
# employee4 = Employee("Brandon", "Smith", "sbrandon@mail.com", "Sales")
# # admin1 = Admin("Monika", "Justine", "jmonika@mail.com", "Marketing")
# teamLead1 = TeamLead("Michael", "Specter", "smichael@mail.com", "Sales")
# req1 = Request("New hire orientation", teamLead1, "27-Jul-2021")
# req2 = Request("Laptop repair", employee1, "1-Jul-2021")
